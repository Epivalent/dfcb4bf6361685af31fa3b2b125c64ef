To achieve this, you can use the `bcc` library, which provides Python bindings for BPF. Below is a concise Python script that registers a BPF filter to block `rt_sigaction` for a specified process by PID and specified signal by name or number.

First, ensure you have the `bcc` library installed:
```sh
sudo apt-get install bpfcc-tools linux-headers-$(uname -r)
pip install bcc
```

Now, here is the Python script:

```python
from bcc import BPF
import ctypes

# Define the BPF program
bpf_program = """
#include <uapi/linux/ptrace.h>
#include <linux/sched.h>

BPF_HASH(target_pid, u32);
BPF_HASH(target_signal, int);

int block_rt_sigaction(struct pt_regs *ctx) {
    u32 pid = bpf_get_current_pid_tgid() >> 32;
    int sig = PT_REGS_PARM2(ctx);

    // Check if current process PID matches target PID
    if (target_pid.lookup(&pid)) {
        // Check if signal matches target signal
        if (target_signal.lookup(&sig)) {
            return -1; // Block rt_sigaction call
        }
    }

    return 0; // Allow rt_sigaction call
}
"""

# Load and attach the BPF program to the rt_sigaction syscall entry point
b = BPF(text=bpf_program)
b.attach_kprobe(event="sys_rt_sigaction", fn_name="block_rt_sigaction")

# Specify target PID and signal number/name (e.g., SIGTERM=15)
TARGET_PID = 1234  # Replace with your target process ID
TARGET_SIGNAL = 15 # Replace with your target signal number

# Update BPF hash maps with target values
b["target_pid"][ctypes.c_uint(TARGET_PID)] = ctypes.c_int(1)
b["target_signal"][ctypes.c_int(TARGET_SIGNAL)] = ctypes.c_int(1)

print(f"Blocking rt_sigaction for PID {TARGET_PID} and Signal {TARGET_SIGNAL}")

try:
    while True:
        pass  # Keep running to maintain attachment of kprobe

except KeyboardInterrupt:
    print("Detaching kprobe and exiting...")
```

### Explanation:

1. **BPF Program**: The eBPF program defined in C-like syntax checks if the current process's PID matches the specified target PID and whether the signal being set matches the specified target signal. If both conditions are met, it blocks (`return -1`) the `rt_sigaction` syscall.

2. **Loading & Attaching**: The script loads this eBPF program into the kernel using `bcc.BPF`, then attaches it to the entry point of `sys_rt_sigaction`.

3. **Target Values**: The script updates two hash maps in eBPF with keys as `PID` and `signal`. These are used within our eBPF program to determine whether to block or allow calls.

4. **Running**: The script runs indefinitely until interrupted by a keyboard interrupt (`Ctrl+C`). This keeps our kprobe attached.

Replace `TARGET_PID` and `TARGET_SIGNAL` with appropriate values for your use case before running this script.

# Tue  2 Jul 16:31:06 CEST 2024 - okay, let's use eBPF instead. write a concise python script that registers a BPF filter which has the effect of blocking rt_sigaction for a specified process by pid and specified signal by name or number